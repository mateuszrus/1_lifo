#include "stdafx.h"
#include <iostream>
#include <locale.h>
#include <cmath>

using namespace std;

int tableSize = 0;
int element;
int table[10];

void AddToLifo()
{
	cout << "Wprowadz cyfre z zakresu od -5 do 5: ";
	cin >> element;

	if (element < -5 || element > 5)
	{
		cout << "Poda�es liczb� z poza zakresu!" << endl << endl;
	}
	else
	{
		if (tableSize >= 0 && tableSize < 10)
		{
			if (tableSize == 0)
			{
				table[0] = element;

				tableSize++;
			}
			else
			{
				table[tableSize] = element;

				tableSize++;
			}
			cout << "Doda�es " << element << endl << endl;
		}
		else
		{
			cout << "Nie ma ju� miejsca na stosie!" << endl << endl;
		}
	}
}

void GetFromLifo()
{
	if (tableSize > 0)
	{
		cout << "Pobrany element to: " << table[tableSize - 1] << endl << endl;
		tableSize--;
	}
	else
	{
		cout << "Stos nie zawiera element�w!" << endl << endl;
	}
}

void ShowLifo()
{
	cout << endl << "Wszystkie elementy stosu: " << endl;

	if (tableSize > 0)
	{
		for (int i = 0; i < tableSize; i++)
		{
			cout << "Element numer " << i + 1 << ": " << table[i] << endl;
		}
	}
	else
	{
		cout << "Brak element�w na stosie" << endl << endl;
	}
}

void SortLifo()
{
	if (tableSize > 0)
	{
		int pmin;
		for (int j = 0; j < tableSize - 1; j++)
		{
			pmin = j;
			for (int i = j + 1; i < tableSize; i++)
			{
				if (table[i] < table[pmin])
				{
					pmin = i;
				}
			}
			swap(table[pmin], table[j]);
		}

		cout << endl << "Posortowano stos!" << endl;
	}
	else
	{
		cout << endl << "Stos nie zawiera elementow do posortowania " << endl;
	}
}

int _tmain(int argc, _TCHAR* argv[])
{
	char typeOfOperation;

	setlocale(LC_ALL, "polish");

	cout << "---MENU---" << endl;
	cout << "1. Dodaj do stosu" << endl;
	cout << "2. Pobierz ze stosu" << endl;
	cout << "3. Wy�wietl stos" << endl;
	cout << "4. Sortuj stos" << endl;

	cout << "Wybierz typ operacji: ";

	while (cin >> typeOfOperation)
	{
		switch (typeOfOperation)
		{
		case '1':
			AddToLifo();
			cout << "Wybierz typ operacji: ";
			break;

		case '2':
			GetFromLifo();
			cout << "Wybierz typ operacji: ";
			break;

		case '3':
			ShowLifo();
			cout << endl << "Wybierz typ operacji: ";
			break;

		case '4':
			SortLifo();
			cout << "Wybierz typ operacji: ";
			break;

		default:
			cout << "Nie ma takiej opcji w MENU!" << endl;
			cout << "Wybierz typ operacji: ";
			break;
		}
	}

	getchar();
	return 0;
}


